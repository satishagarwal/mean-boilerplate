module.exports = function(app) {
	
	// route for home page
	app.get('/', function(req, res) {
		res.sendfile('./client/views/index.html'); // supply full path cause sendfile does not use static folder concept.
		
	});
};