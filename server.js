// server.js

// modules =================================================
var express        = require('express');
var app            = express();
var mongoose       = require('mongoose');
var bodyParser     = require('body-parser');

var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan'); //Logger middleware

// configuration ===========================================

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// config files
var db = require('./server/config/database');

var port = process.env.PORT || 8080; // set our port

// connect to our mongoDB database (uncomment after you enter in your own credentials in config/db.js)
//mongoose.connect(db.url);

app.use(morgan('dev')); //Log all http request to console.
app.use(cookieParser());
app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


app.use(session({ secret : 'meanboilerplateapplicationfordevelopers'}));

// routes ==================================================
require('./server/routes')(app); // configure our routes
//require('./server/rest')(app,mongoose);

app.set('views', __dirname + '/client/views');
app.use(express.static(__dirname + '/client'));

// start app ===============================================
var server = require('http').Server(app);

server.listen(port);										// startup our app at http://localhost:8080
console.log('Magic happens on port ' + port); 			// shoutout to the user
exports = module.exports = app; 						// expose app