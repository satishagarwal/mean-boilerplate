angular.module('appRoutes', [])

.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		.when('/search', {
			templateUrl: '/views/search.html',
			controller:'SearchController'
		})
		
				
		.otherwise({
			redirectTo : '/'
		})

	$locationProvider.html5Mode(true);

}]);